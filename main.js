let timer;

document.addEventListener('input', e => {
const el = e.target;

if( el.matches('[data-color]') ) {
    clearTimeout(timer);
    timer = setTimeout(() => {
    document.documentElement.style.setProperty(`--color-${el.dataset.color}`, el.value);
    }, 100)
}
})

var inputElement = document.getElementById("myInput");
inputElement.readOnly = true;

const pasteButton = document.getElementById("pasteButton");
const textToPasteInput = document.getElementById("textToPaste");

// Add a click event listener to the button
pasteButton.addEventListener("click", async () => {
try {
    // Use the Clipboard API to paste text from the clipboard
    const clipboardText = await navigator.clipboard.readText();
    textToPasteInput.value = clipboardText;
} catch (error) {
    console.error("Failed to paste text: " + error);
}
});


const copyButton = document.getElementById('copyButton');

copyButton.addEventListener('click', () => {
    inputElement.select();
    document.execCommand('copy');
    console.log('Text copied to clipboard: ' + inputElement.value);
});

// const convert_btn = document.getElementById('convert_btn');
$("#convert_btn").on("click", function(event) {
    console.log("btn cleck")
    convertText()
});

function convertText() {
    const textInput = document.getElementById('textToPaste').value;
    const resultArea = document.getElementById('myInput');
    const englishLayout = "qwertyuiop[]asdfghjkl;'zxcvbnm,.";
    const russianLayout = "йцукенгшщзхъфывапролджэячсмитьбю";

    const detectLayout = (text) => {
        const russianChars = new Set(russianLayout.split(''));
        const englishChars = new Set(englishLayout.split(''));
        const textChars = Array.from(text.toLowerCase());
    
        let isRussian = true;
        let isEnglish = true;
    
        for (const char of textChars) {
            if (russianChars.has(char)) {
                isEnglish = false;
            } else if (englishChars.has(char)) {
                isRussian = false;
            }
        }
    
        if (isRussian) {
            return 'russian';
        } else if (isEnglish) {
            return 'english';
        } else {
            return 'unknown'; // Mixed characters, cannot determine layout
        }
    };
    

    const convertToRussian = (text) => {
        const conversionMap = {};
        for (let i = 0; i < englishLayout.length; i++) {
            conversionMap[englishLayout[i]] = russianLayout[i];
        }
        return Array.from(text)
            .map(char => conversionMap[char] || char)
            .join('');
    };

    const convertToEnglish = (text) => {
        const conversionMap = {};
        for (let i = 0; i < russianLayout.length; i++) {
            conversionMap[russianLayout[i]] = englishLayout[i];
        }
        return Array.from(text)
            .map(char => conversionMap[char] || char)
            .join('');
    };

    const layout = detectLayout(textInput);
    let convertedText = '';

    if (layout === 'russian') {
        convertedText = convertToEnglish(textInput);
    } else if (layout === 'english') {
        convertedText = convertToRussian(textInput);
    } else {
        convertedText = 'Cannot determine layout. Please enter text in one layout only.';
    }

    resultArea.value = convertedText;
}
